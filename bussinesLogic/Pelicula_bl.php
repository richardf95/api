<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Pelicula_bl{

  

     public static function get($id){
       $pelicula = Pelicula::where("idPelicula", $id);
                   print_r($pelicula);
          if($pelicula != null){
              header('Content-Type: application/json');
              return json_encode($pelicula);
          } else{
              $err = ['error' =>'No se encontró una pelicula'];
              header('Content-Type: application/json');
              return json_encode($err);
          }
    }
    
 	public static function getPeliculaCategoria($descripcion){
 		$categoria = new Categoria();
 		$categoria= $categoria->where("descripcion",$descripcion);
                $cat_ser = PeliculaCategoria::getBy("idCategoria",$categoria[0]['idCategoria']);
                if(count($cat_ser)==1){
                    $pelicula = Pelicula::whereR('*','idPelicula',$cat_ser->getId(),'PeliculaCategoria');
                    return $pelicula;
                } else if(count($cat_ser)>1){
                    $array = array();
                    for($i=0;$i<count($cat_ser);$i++){
                        $peliculas = Pelicula::getById($cat_ser);
                    array_push($array, $peliculas);
                    }
                    return $array;
                }

 	}
    
        
        	public static function getPeliculasCategorias(){
 		$categoria = new Categoria();
 		$categoria= $categoria->getAll();
                 return json_encode($categoria);
                

 	}
    
    
    public static function getCategoria($id){
          $pelicula = Pelicula::whereR('idCategoria', 'idPelicula', $id, 'PeliculaCategoria');
        
        if($pelicula != null){
            header('Content-Type: application/json');
            return json_encode($pelicula);
        } else{
            $err = ['error' =>'No se encontró una pelicula'];
            header('Content-Type: application/json');
            return json_encode($err);
        }
    }
    

    public static function getAll(){
        $pelicula = Pelicula::getAll();
        print_r($pelicula);
         return json_encode($pelicula);
    }
    public static function getAllFrom($table){
        $pelicula = Pelicula::getAllFrom($table);
        print_r($pelicula);
         return json_encode($pelicula);
    }


    public static function create(Pelicula $pelicula){
      //verifica que el titulo no hay asido agregado ya


        if(self::exists($pelicula->getTitulo())==0){

          $instanciate= Pelicula::getInstance($pelicula);
          $instanciate->create();

          //print_r($instanciate);
          $r["error"] = 0;
          $r["mensaje"] = "Todo Correcto";
          print_r($r);

          }else{
            $r["error"] = 1;
            $r["mensaje"] = "Esa Pelicula ya Existe";
          print_r($r);

        }

    }

    public static function update(Pelicula $pelicula){
  
      $r = $pelicula->update('idPelicula');
        
      return $r;
    }
 
    
     public static function exists($title){

      $pelicula = Pelicula::getBy('titulo', $title);

      if($pelicula != null){
      	$tituloServer = strtolower($pelicula->getTitulo());
      	$title = strtolower($title);
        if($tituloServer == $title){
          $r = 1;
        }
      }else {
        $r = 0;
      }
      return $r;

    }


    public static function delete($id){
     //Borrado de la lista dereproduccion
    $pelicula = Pelicula::whereR('idListaDetalle', 'idPelicula', $id, 'ListaDetalle');


     if(count($pelicula) != 0){
     $nuevoId = $pelicula[0]['idListaDetalle'];
     Pelicula::deleteCompuesto('ListaDetalle',$nuevoId,'idListaDetalle');
     }
    //Borrado de la pelicula categoria
    $pelicula = Pelicula::whereR('idPelicula', 'idPelicula', $id, 'PeliculaCategoria');

      if(count($pelicula) != 0){
       $nuevoId = $pelicula[0]['idPelicula'];
        Pelicula::deleteCompuesto('PeliculaCategoria',$nuevoId,'idPelicula');
     }
     //Borrado de la pelicula
    $pelicula = Pelicula::whereR('idPelicula', 'idPelicula', $id, 'Pelicula');
     $nuevoId = $pelicula[0]['idPelicula'];
     Pelicula::deleteById($nuevoId,'idPelicula');


    }


}
